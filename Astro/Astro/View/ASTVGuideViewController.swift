//
//  ASTVGuideViewController.swift
//  Astro
//
//  Created by NHT on 9/20/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import SpreadsheetView

class ASTVGuideViewController: UIViewController {

    @IBOutlet private weak var spreadsheetView: SpreadsheetView!
    fileprivate var viewModel: ASTVGuideViewModel!
    
    fileprivate let numberOfRows = 24 * 2 + 1 + 1
    fileprivate let oneHourHeight: CGFloat = 60
    
    fileprivate let hourFormatter = DateFormatter()
    fileprivate let twelveHourFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSpreadsheet()
        configViewModel()
    }
    
    private func configViewModel() {
        viewModel = ASTVGuideViewModel()
        viewModel.getChannelList()
        viewModel.channelListDidChange = { [weak self] viewModel in
            self?.spreadsheetView.reloadData()
        }
    }
        
    private func configSpreadsheet() {
        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self
        
        spreadsheetView.register(HourCell.self, forCellWithReuseIdentifier: String(describing: HourCell.self))
        spreadsheetView.register(ChannelCell.self, forCellWithReuseIdentifier: String(describing: ChannelCell.self))
        spreadsheetView.register(UINib(nibName: String(describing: SlotCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SlotCell.self))
        spreadsheetView.register(BlankCell.self, forCellWithReuseIdentifier: String(describing: BlankCell.self))
        
        spreadsheetView.backgroundColor = .black
        
        let hairline = 1 / UIScreen.main.scale
        spreadsheetView.stickyRowHeader = true
        spreadsheetView.stickyColumnHeader = true
        spreadsheetView.intercellSpacing = CGSize(width: hairline, height: hairline)
        spreadsheetView.gridStyle = .solid(width: hairline, color: .lightGray)
        
        hourFormatter.calendar = Calendar(identifier: .gregorian)
        hourFormatter.locale = Locale(identifier: "en_US_POSIX")
        hourFormatter.dateFormat = "h\na"
        
        twelveHourFormatter.calendar = Calendar(identifier: .gregorian)
        twelveHourFormatter.locale = Locale(identifier: "en_US_POSIX")
        twelveHourFormatter.dateFormat = "HH:mm"
    }

    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        viewModel.sortChannelByType(SortType(rawValue: sender.selectedSegmentIndex))
        spreadsheetView.reloadData()
    }
    
}

extension ASTVGuideViewController: SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    // MARK: DataSource
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return viewModel.channelList?.count ?? 0 + 1
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return numberOfRows
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 39
        }
        return 120
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return 20
        }
        return oneHourHeight
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if indexPath.column == 0 && indexPath.row == 0 {
            return nil
        }
        
        if indexPath.column == 0 && indexPath.row > 0 { // time
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: HourCell.self), for: indexPath) as! HourCell
            let time = Float(indexPath.row - 1) * 0.5
            let timeString = String(format: "%.0f:%02d", Float(Int(time)), Int((time - Float(Int(time))) * 60) )

            cell.label.text = timeString
            cell.gridlines.top = .solid(width: 1, color: .white)
            cell.gridlines.bottom = .solid(width: 1, color: .white)
            cell.label.backgroundColor = viewModel.getCurrentMinutes() < Int(time*60) ? ASColor.timeColor : .gray
            return cell
        }
        
        let channel = viewModel.channelList?[indexPath.column]
        channel?.eventListDidChange = { viewModel in
            spreadsheetView.reloadData()
        }
        
        if indexPath.column > 0 && indexPath.row == 0 { // channel name
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: ChannelCell.self), for: indexPath) as! ChannelCell
            cell.label.text = channel?.channelTitle
            cell.gridlines.top = .solid(width: 1, color: .black)
            cell.gridlines.bottom = .solid(width: 1, color: .black)
            cell.gridlines.left = .solid(width: 1 / UIScreen.main.scale, color: UIColor(white: 0.3, alpha: 1))
            cell.gridlines.right = cell.gridlines.left
            return cell
        }
        
        if let eventList = channel?.getEventList(date: Date()), indexPath.row < eventList.count { // event
            let event = eventList[indexPath.row]
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SlotCell.self), for: indexPath) as! SlotCell
            cell.minutes = viewModel.getEventTimeStart(event: event)
            cell.title = event.programmeTitle
            cell.tableHighlight = event.shortSynopsis
            cell.backgroundColor = viewModel.getCurrentMinutes() < viewModel.getEventStartMinutes(event: event) ? .white : .lightGray
            return cell
        }
        return spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: BlankCell.self), for: indexPath)
    }
    
    /// Delegate
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
    }
    
}
