//
//  ASChannelListViewController.swift
//  Astro
//
//  Created by NHT on 9/19/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import MJRefresh
import DZNEmptyDataSet
import FirebaseAuth

enum SortType: Int {
    case AtoZ, ZtoA, ChannelNumberASC, ChannelNumberDESC
}

class ASChannelListViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    fileprivate var viewModel: ASChannelListViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configViewModel()
        configTableView()
    }
    
    private func configViewModel() {
        viewModel = ASChannelListViewModel()
        viewModel.channelListDidChange = { [weak self] viewModel in
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.reloadData()
        }
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                 self.viewModel.getChannelList()
            }
        }
    }

    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        viewModel.sortChannelByType(SortType(rawValue: sender.selectedSegmentIndex))
    }
    
    /// Config load more and pull to refresh
    private func configTableView() {
        // empty case
        tableView.emptyDataSetSource = self
        
        // pull to refresh
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: {
            self.viewModel.getChannelList()
        })
    }
    
}

extension ASChannelListViewController: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.channelList?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelCell", for: indexPath)
        let channel = viewModel.channelList?[indexPath.row]
        cell.textLabel?.text = channel?.channelTitle
        cell.detailTextLabel?.text = channel?.channelStbNumber
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let channel = viewModel.channelList?[indexPath.row] else {
            return
        }

        if AccessToken.current != nil {
            confirmAndAddFavorite(channel: channel)
        } else {
            viewModel.loginFacebook(viewController: self, finish: { [weak self] (isSuccess, loginResult) in
                if isSuccess {
                    self?.confirmAndAddFavorite(channel: channel)
                }
            })
        }
    }
    
    private func confirmAndAddFavorite(channel: Channel) {
        ASAlert.confirmYesNoAction(message: "Add this channel to favorite ?") { [weak self](isAccept) in
            self?.viewModel.saveChannelAsFavorite(channel)
            self?.viewModel.saveChannelAsFavoriteOnFireBase(channel)
        }
    }    

}

// MARK: Handle for Empty data case
extension ASChannelListViewController: DZNEmptyDataSetSource {
    
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 12.0), NSForegroundColorAttributeName : UIColor.blue]
        return NSAttributedString(string: LocalizedString("NO_DATA"), attributes: attributes)
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "movie-banner-wide")
    }
    
}
