//
//  ASChannelFavoriteViewController.swift
//  Astro
//
//  Created by NHT on 9/19/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ASChannelFavoriteViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var sortSegment: UISegmentedControl!
    fileprivate var viewModel: ASFavoriteChannelViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ASFavoriteChannelViewModel()
        viewModel.channelListDidChange = { [weak self] viewModel in
            self?.tableView.reloadData()
        }
        tableView.emptyDataSetSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getChannelFavoriteOnFireBase(SortType(rawValue: sortSegment.selectedSegmentIndex))
    }
    
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        viewModel.sortChannelByType(SortType(rawValue: sender.selectedSegmentIndex))
    }

    @IBAction func signout(_ sender: Any) {
        ASAlert.confirmYesNoAction(message: "Do you want to sign out ?") { [weak self] (isAccept) in
            self?.viewModel.signOut()
        }
    }
    
}

extension ASChannelFavoriteViewController: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.channelList?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelCell", for: indexPath)
        let channel = viewModel.channelList?[indexPath.row]
        cell.textLabel?.text = channel?.channelTitle
        cell.detailTextLabel?.text = channel?.channelStbNumber
        return cell
    }
    
}

// MARK: Handle for Empty data case
extension ASChannelFavoriteViewController: DZNEmptyDataSetSource {
    
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 12.0), NSForegroundColorAttributeName : UIColor.blue]
        return NSAttributedString(string: LocalizedString("NO_DATA"), attributes: attributes)
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "movie-banner-wide")
    }
    
}
